" ---------------------------------
" ---- GVIM CONFIGURATION FILE ----
" ---------------------------------


set guioptions-=T 
" MAPPINGS "{{{1
nmap // \ll     " Compilation Latex
nmap /+ \lv     " Visualisation fichier PDF  

set laststatus=2
set bsdir=last
set cd=$PATH,$HOME
set lines=44 columns=120

set guifont=Monaco:h12

" === COLORS ==="{{{1
colorscheme nucolors
hi FoldColumn guifg=greenyellow guibg=#202020


" vim: fdm=marker:
