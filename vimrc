"==============================================================================
" My personal vim configuration file           
"
" Maintainer:   Taurus Olson                   
"==============================================================================

set nocompatible
filetype off

"=== Pathogen ============================================================={{{1

call pathogen#runtime_append_all_bundles()
call pathogen#helptags()

"=== VundleConfig ========================================================={{{1

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" OPTIONS ================================================================={{{1

syntax on
"filetype on
filetype plugin indent on

" Tags ===================================================================={{{2

set tagstack          
set tags=./tags,tags

" Windows ================================================================={{{2

set splitright
set nosplitbelow

" Display ================================================================={{{2

set encoding=utf-8
set ttyfast
set vb t_vb=                    " No sound
set mouse=a                     " In many terminal emulators the mouse works 
set t_Co=256

" Informations ============================================================{{{2

set laststatus=2                " Affiche la barre de statut
set ruler                       " Show the cursor position all the time
set showcmd                     " Display incomplete commands
set completeopt=menuone,longest " Autocompletion
set modelines=5                
set hlsearch

" Completion =============================================================={{{2

set wildmenu
set wildmode=list:full
set backspace=indent,eol,start  " Allow backspacing over everything in insert mode
set wildignore+=.hg,.git,.svn                    " Version control
set wildignore+=*.aux,*.out,*.toc                " LaTeX intermediate files
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg   " binary images
set wildignore+=*.luac                           " Lua byte code
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest " compiled object files
set wildignore+=*.pyc                            " Python byte code
set wildignore+=*.spl                            " compiled spelling word lists
set wildignore+=*.sw?                            " Vim swap files
set wildignore+=*.DS_Store?                      " OSX bullshit

" Indentation ============================================================={{{2

set cindent             " Indentation plus stricte pour C
set autoindent          " equals key
set expandtab           " insert mode: tab to spaces conversion
set shiftwidth=4        " nb of spaces to use for each step of indent
set smarttab 
set tabstop=4 
set softtabstop=4 

" Automatic ==============================================================={{{2

set history=50          
set autowrite           
set wrap                
set linebreak
set nolist
set scrolloff=3         
set virtualedit=block,onemore
set hidden
set formatoptions=qrn1
set incsearch           
set number

" Undo ===================================================================={{{2

set undofile
set undodir=/tmp

" Laststatus =============================================================={{{2

set statusline =
set statusline +=[%n]
set statusline +=%<%f\ %h%m%r
set statusline +=%y                                                  " FileType
" set statusline +=\ [%{strlen(&fenc)?&fenc:&enc}]                   " Encoding
set statusline +=%20.{strftime(\"%d-%m-%y\|%T\",getftime(expand(\"%:p\")))} 
set statusline +=%30.{fugitive#statusline()}
set statusline +=%=%-10L
set statusline +=%=%-14.(%l,%c%V%)\ %P

" Highlight VCS conflict markers
match ErrorMsg '^\(<\|=\|>\)\{7\}\([^=].\+\)\?$'

" MAPPINGS ================================================================{{{1

"==Open files=="
map ,v :e ~/.vimrc<CR>
map ,u :source ~/.vimrc<CR>
map !b :new ~/.bookmarks_vim.bm<CR> 
map ,t <C-]>
nmap ;b :e ~/.bashrc<CR>
nmap ;z :e ~/.zshrc<CR>

" Formatting
nnoremap Q gq

" Don't move on *
nnoremap * *<C-o>

" Yank until end of line
nnoremap Y y$ 

" Add lines in NORMAL mode 
nnoremap ,o A<CR><ESC>
nnoremap ,O I<CR><ESC>k

" Searching with <C-r> in VISUAL mode 
vnoremap <C-r> "hy:%s/<C-r>h//gc<left><left><left>

" Change the current working dir to the dir of current file
nnoremap ,cd :cd %:p:h <CR>

" Quick mapping to put \(\) in COMMAND mode (usefull for regexes)
cmap ;\ \(\)<Left><Left>

" Mappings to go faster
inoremap kj <ESC>
nnoremap ,, :w<CR>

" Snippets {{{2
inoremap ( ()<C-[>i
inoremap [ []<C-[>i
inoremap { {}<C-[>i

" Buffers {{{2
nnoremap <C-J> :bp<CR>
nnoremap <C-K> :bn<CR>
nnoremap ,b :b#<CR>

" vimgrep
nnoremap \g :vimgrep // %<left><left><left>
nnoremap <silent> \o :copen<CR>:setlocal cul<CR>
nnoremap ;) :cnext<CR>
nnoremap ;( :cprev<CR>

" Toggle paste
map =p :set paste!<CR>

" Windows "{{{2
nnoremap + <C-W>+
nnoremap - <C-W>-
nnoremap ;< <C-W><
nnoremap ;> <C-W>>
nnoremap <C-C>l <C-W>l:q<CR>
nnoremap <C-C>h <C-W>h:q<CR>
nnoremap <C-C>j <C-W>j:q<CR>
nnoremap <C-C>k <C-W>k:q<CR>

" Tabs  {{{2
nnoremap ]t :tabnext<CR>
nnoremap [t :tabprev<CR>
nnoremap ]T :tabnew<CR>
nnoremap [T :tabedit<CR>

"Date writing (instead of  _date_ )
nmap ,date :s/_date_/\=strftime("%c")/ 

" Fold marker 
nmap \1f A<SPACE>{{{<ESC>lC1<ESC>
nmap \2f A<SPACE>{{{<ESC>lC2<ESC>
nmap \3f A<SPACE>{{{<ESC>lC3<ESC>

" TEMPLATES ==============================================================={{{1

autocmd Filetype javascript nmap \t :r ~/Documents/Templates/protovis.tmpl

" PLUGINS ================================================================={{{1

"=== Vim-latex ============================================================{{{2

filetype plugin on
set grepprg=grep\ -nH\ $*

" Vim-Latex variables
let g:tex_flavor = "latex"
let Tex_ViewRuleComplete_pdf = '/usr/bin/open -a Preview $*.pdf' 

" Vim-Latex mappings
imap <buffer> <leader>it <Plug>Tex_InsertItemOnThisLine
nmap ,map :vnew ~/.vim/ftplugin/tex_macros.vim<CR>
nmap ,ê :%s/ê/\\`{e}/g<CR>
nmap ,é :%s/é/\\'{e}/g<CR>
nmap ,è :%s/è/\\`{e}/g<CR>
nmap // \ll     " Compilation Latex

"=== MRU =================================================================={{{2

"Bundle "vim-scripts/mru.vim"

noremap ;m :MRU<cr> 
map ,m :e ~/.vim_mru_files<CR>

"=== Markdown ============================================================={{{2

Bundle "tpope/vim-markdown"

augroup md
    autocmd BufRead *.md  set ai formatoptions=tcroqn2 comments=n:>
augroup END

"=== SnipMate ============================================================={{{2

nmap ,s :new ~/.vim/bundle/snipmate/snippets<CR>

"=== TagList =============================================================={{{2

Bundle "vim-scripts/taglist.vim"

map ;t :TlistToggle<CR>
let Tlist_Show_One_File = 1     " Show only the tags of the current buffer
let Tlist_Show_Menu = 1
let Tlist_Ctags_Cmd = '/opt/local/bin/ctags'
let Tlist_Use_Right_Window = 1

"=== IMAPS ================================================================{{{2
" augroup myIMAPs
"     au!
"     source ~/.vim/myIMAPs.txt
" augroup END

"=== NERDTree ============================================================={{{2

let NERDTreeWinPos='left'  
let NERDTreeShowBookmarks=1   
let g:NERDTreeHijackNetrw=0
nmap ;n :NERDTreeToggle<CR>
"nmap ;f :NERDTreeFind<CR>

"=== NetRW ================================================================{{{2

map ;e :Explore<CR>
map ;x :Sexplore<CR>
map ;v :Vexplore<CR>
let g:netrw_liststyle=3 " (tree listing)
let g:netrw_winsize=30

"=== gundo.vim ============================================================{{{2

Bundle "sjl/gundo.vim"

nnoremap ;g :GundoToggle<CR>

"=== Tagbar ==============================================================={{{2

"Bundle "majutsushi/tagbar"

let g:tagbar_ctags_bin='/opt/local/bin/ctags'

"=== Unite ================================================================{{{2

Bundle "Shougo/unite.vim"

nmap ;uf :Unite file<CR>
nmap ;ub :UniteBookmarkAdd<space>
nmap \\ :Unite buffer<CR>

"=== Vundle =============================================================={{{2

Bundle "gmarik/vundle"

"=== vim-quickrun ========================================================={{{2

Bundle "thinca/vim-quickrun"

"=== vim-fugitive ========================================================={{{2

Bundle "tpope/vim-fugitive"

"=== vim-git =============================================================={{{2

Bundle "tpope/vim-git"

"=== vim-repeat ==========================================================={{{2

Bundle "tpope/vim-repeat"

"=== vim-org-mode ========================================================={{{2

let g:org_agenda_files = ['~/Dropbox/org/TODO.org', '~/Dropbox/org/notes.org']


"=== FuzzyFinder =========================================================={{{2

Bundle 'L9'
Bundle 'FuzzyFinder'

nnoremap ;ff :FufFile<CR>
nnoremap ;fb :FufBuffer<CR>
nnoremap ;fd :FufDir<CR>
nnoremap ;fm :FufMruFile<CR>
nnoremap ;fkf :FufBookmarkFile<CR>
nnoremap ;fkd :FufBookmarkDir<CR>
nnoremap ;fj :FufJumpList<CR>

" FUNCTIONS ==============================================================={{{1

" InsertTabWrapper"{{{2
" May cause conflicts with snipMate snippets using <TAB>
function! InsertTabWrapper(direction)
    let col = col('.') - 1
    if !col || getline('.')[col - 1] !~ '\k'
        return "\<tab>"
    elseif "backward" == a:direction
        return "\<c-p>"
    else
        return "\<c-n>"
    endif
endfunction
inoremap <S-tab> <c-r>=InsertTabWrapper ("forward")<CR>
inoremap <C-tab> <c-r>=InsertTabWrapper ("backward")<CR>

" MoshBookmark "{{{2
function! MoshBookmark()
    redir >> ~/.bookmarks_vim.bm
    echo
    echo strftime("%Y-%b-%d %a %H:%M")
    echo "cd ". $PWD
    echo "vim ". expand("%:p").' '.line('.')
    echo ' word='.expand("<cword>")
    echo ' cline='.getline('.')
    echo ''
    redir END
endfunction
:command! MoshBookmark :call MoshBookmark()


" Todo List "{{{2
function! OpenTodoListWin(...)
   " Variables
   let splitcmd='vnew'
   let editcmd='e ./todo.otl'
   let s:split_width=48
   let s:split_minwidth=1
   let splitcmd=s:split_width.splitcmd
   " Execution
   execute splitcmd
   execute "setlocal wiw=".s:split_minwidth
   execute editcmd
endfunction
command! -nargs=* -complete=dir OpenTodoListWin call OpenTodoListWin(<f-args>)

" Filetype_tex "{{{2
function! Filetype_tex()
    if (! filereadable("Makefile"))
        "set makeprg=latex\ %
        exec "!dvipdf %<.dvi"
        echo "Press ENTER to continue"
        exec "!open %<.pdf"
    endif
endfunction
command! Dvi2PDF :call Filetype_tex()

" SYNTAX =================================================================={{{1
    
" Save when losing focus
au FocusLost * :wa

" Source vimrc after writing the whole buffer to a file 
" autocmd BufWritePost .vimrc source $MYVIMRC

" Resize splits when the window is resized
au VimResized * exe "normal! \<c-w>="

" Delete Fugitive buffer when leaving it
autocmd BufReadPost fugitive://* set bufhidden=delete

" LATEX ""{{{2
"======="
autocmd Filetype set sw=4 textwidth=79
autocmd Filetype tex let @p="\\partial"          " \partial
autocmd Filetype tex let @s="i$f i$"         " $$
autocmd Filetype tex let @v="i\\vec\<SPACE>\<ESC>" "\vec
autocmd Filetype tex nmap ;t :new ~/Latex/template/<CR>
autocmd FileType tex set makeprg=make
" Snippets
autocmd FileType tex nmap ;s :new ~/.vim/bundle/snipmate/snippets/tex.snippets<CR>
autocmd FileType tex imap ( ()<C-[>i
autocmd FileType tex imap [ []<C-[>i
autocmd FileType tex imap { {}<C-[>i
autocmd FileType tex imap _ _{<right>
autocmd FileType tex imap ^ ^{<right>
autocmd FileType tex set commentstring=%\ %s

" PYTHON ""{{{2
" ======="
" Autocompletion
autocmd FileType python set commentstring=#\ %s
autocmd FileType python set textwidth=79 
autocmd FileType python set omnifunc=pythoncomplete#Complete
" Snippets
autocmd FileType python nmap ;p :new ~/programmation/python/Python_snippets/<CR>
autocmd FileType python nmap ;s :new ~/.vim/bundle/snipmate/snippets/python.snippets<CR>
" Options
let python_highlight_all = 1


" PERL ""{{{2
" ====="
" Autocompletion
" autocmd FileType perl set omnifunc=perlcomplete#Complete 
" Snippets
autocmd FileType perl set commentstring=#\ %s
autocmd FileType perl nmap ;p :new ~/programmation/perl/Perl_snippets/<CR>
autocmd FileType perl nmap ;s :new ~/.vim/bundle/snipmate/snippets/perl.snippets<CR>
let perl_fold = 1
let perl_fold_blocks = 1

" C ""{{{2
" = " 
" Snippets
autocmd FileType c set commentstring=//\ %s
autocmd FileType c nmap ;s :new ~/.vim/bundle/snipmate/snippets/c.snippets<CR>
autocmd FileType c set makeprg=gcc\ -o\ %<\ %	" Compile fichier C
autocmd FileType c set omnifunc=ccomplete#Complete

" FORTRAN ""{{{2
" ======= " 
" Snippets
autocmd FileType fortran nmap ;s :new ~/.vim/bundle/snipmate/snippets/fortran.snippets<CR>
autocmd FileType fortran set makeprg=gfortran\ -o\ %<\ %

" RUBY ""{{{2
" ====="
" Snippets
autocmd FileType ruby set commentstring=#\ %s
autocmd FileType ruby nmap ;s :new ~/.vim/bundle/snipmate/snippets/ruby.snippets<CR>

" HTML ""{{{2
" ====="
autocmd FileType html imap < <><C-[>i

" PROCESSING ""{{{2
" ========== "
" Snippets
autocmd Filetype processing set sw=4 textwidth=79
autocmd FileType processing set commentstring=//\ %s
autocmd FileType processing nmap ;s :new ~/.vim/bundle/snipmate/snippets/processing.snippets<CR>

" BASH ""{{{2
" ==== "
autocmd FileType shell set commentstring="#\ %s"
let g:sh_fold_enabled= 1    "(enable function folding)
let g:sh_fold_enabled= 2    "(enable heredoc folding)
let g:sh_fold_enabled= 4    "(enable if/do/for folding)


" JAVA ""{{{2
" ==== "
autocmd FileType java set makeprg=javac\ %
autocmd FileType java set commentstring=//\ %s

" MARKDOWN "{{{2
" ======== "
" au BufNewFile,BufRead *.md FoldMatching ^#\s \-\s -1

" VORG "{{{2
" ==== "
au BufNewFile,BufRead *.vorg inoremap [ [

" MUSTACHE "{{{2
" ======== "
au BufNewFile,BufRead *.mustache        setf mustache

" YAML "{{{2
" ==== "
autocmd FileType yaml set sw=2


" ABBREVIATIONS ==========================================================={{{1
iabbrev TO Taurus Olson


" vim:set fdm=marker:
