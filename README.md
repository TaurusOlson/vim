# VIM CONFIGURATION FILES

This is my .vim directory. I cleaned it up and removed all the plugins slowing down the the editor.
I tried to keep only those I couldn't work without.


# INSTALLATION

    git clone git://github.com/TaurusOlson/vim.git ~/.vim


Create symlinks:

    ln -s ~/.vim/vimrc ~/.vimrc
    ln -s ~/.vim/gvimrc ~/.gvimrc

Import settings with git submodule:
    
    git submodule init
    git submodule update


# PLUGINS

- Processing
- align
- gundo.vim
- lastchange
- mru.vim
- nerdtree
- project
- snipmate
- surround
- taglist.vim
- unite.vim
- vim-fugitive
- vim-git
- vim-latex
- vim-markdown
- vim-quickrun
- vim-repeat
- vundle
